package ictgradschool.web.lab15.ex01;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by User on 17/05/2017.
 */
public class ImageGalleryDisplay extends HttpServlet {


    public ImageGalleryDisplay() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        //displayBasic(request, response);

        displayHTML(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        Map<String, String> pngNameAndPath = new HashMap<>();
        Map<String, String> jpgNameAndPath = new HashMap<>();

        PrintWriter out = response.getWriter();
        out.println("<html>\n<head><title>Server response</title>");
        out.println("</head>\n<body>");

        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos/");
        File folder = new File(fullPhotoPath);

        System.out.println(folder.getPath());


        FilenameFilter filterPNG = new FilenameFilter() {
            @Override
            public boolean accept(final File dir, final String name) {
                if (name.endsWith("_thumbnail.png")) {
                    return (true);
                }
                return (false);
            }
        };

        FilenameFilter filterJPG = new FilenameFilter() {
            @Override
            public boolean accept(final File dir, final String name) {
                if (name.endsWith(".jpg")) {
                    return (true);
                }
                return (false);
            }
        };


        if (folder.isDirectory()) { // make sure it's a directory
            for (File f : folder.listFiles(filterPNG)) {
                String name = f.getName().substring(0, f.getName().indexOf("_thumbnail.png")).replaceAll("_"," ");
                pngNameAndPath.put(name, "/Photos/"+f.getName());
            }

            for (File f : folder.listFiles(filterJPG)){
                String name = f.getName().substring(0, f.getName().indexOf(".jpg")).replaceAll("_"," ");
                jpgNameAndPath.put(name, f.getPath());
            }


            Iterator<Map.Entry<String, String>> i = pngNameAndPath.entrySet().iterator();

            while(i.hasNext()) {
                Map.Entry<String, String> entry = i.next();
                String name = entry.getKey(); //.toUpperCase();
                String thumbnailPath = entry.getValue();
                String jpgAbsolutePath = jpgNameAndPath.get(name);
                String jpgPath =  "/Photos/" +name.replaceAll(" ", "_")+".jpg";
                Long imageSize = new File(jpgAbsolutePath).length();

                out.println("<p><a href=\""+jpgPath+"\">");
                out.println("<img src=\""+thumbnailPath+"\" alt=\"To original image\">");
                out.println("</a>"+"Name: "+name+". File-size: "+imageSize);
                out.println("</p>");
            }

                out.println("<br>");
            }


            out.println("</body></html>");
        }


    }

